(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#fafafa" "#f07171" "#86b300" "#f2ae49" "#399ee6" "#a37acc" "#55b4d4" "#575f66"])
 '(custom-safe-themes
   '("1d5e33500bc9548f800f9e248b57d1b2a9ecde79cb40c0b1398dec51ee820daf" "234dbb732ef054b109a9e5ee5b499632c63cc24f7c2383a849815dacc1727cb6" "47db50ff66e35d3a440485357fb6acb767c100e135ccdf459060407f8baea7b2" "da186cce19b5aed3f6a2316845583dbee76aea9255ea0da857d1c058ff003546" "f91395598d4cb3e2ae6a2db8527ceb83fed79dbaf007f435de3e91e5bda485fb" "cbdf8c2e1b2b5c15b34ddb5063f1b21514c7169ff20e081d39cf57ffee89bc1e" "b186688fbec5e00ee8683b9f2588523abdf2db40562839b2c5458fcfb322c8a4" "8d7b028e7b7843ae00498f68fad28f3c6258eda0650fe7e17bfb017d51d0e2a2" "d6844d1e698d76ef048a53cefe713dbbe3af43a1362de81cdd3aefa3711eae0d" "846b3dc12d774794861d81d7d2dcdb9645f82423565bfb4dad01204fa322dbd5" "835868dcd17131ba8b9619d14c67c127aa18b90a82438c8613586331129dda63" "a9a67b318b7417adbedaab02f05fa679973e9718d9d26075c6235b1f0db703c8" "850bb46cc41d8a28669f78b98db04a46053eca663db71a001b40288a9b36796c" "f7fed1aadf1967523c120c4c82ea48442a51ac65074ba544a5aefc5af490893b" "745d03d647c4b118f671c49214420639cb3af7152e81f132478ed1c649d4597d" "1704976a1797342a1b4ea7a75bdbb3be1569f4619134341bd5a4c1cfb16abad4" "d268b67e0935b9ebc427cad88ded41e875abfcc27abd409726a92e55459e0d01" "c2aeb1bd4aa80f1e4f95746bda040aafb78b1808de07d340007ba898efa484f5" "97db542a8a1731ef44b60bc97406c1eb7ed4528b0d7296997cbb53969df852d6" "1d44ec8ec6ec6e6be32f2f73edf398620bb721afeed50f75df6b12ccff0fbb15" "5f19cb23200e0ac301d42b880641128833067d341d22344806cdad48e6ec62f6" "4a5aa2ccb3fa837f322276c060ea8a3d10181fecbd1b74cb97df8e191b214313" "4133d2d6553fe5af2ce3f24b7267af475b5e839069ba0e5c80416aa28913e89a" "1278c5f263cdb064b5c86ab7aa0a76552082cf0189acf6df17269219ba496053" "b0e446b48d03c5053af28908168262c3e5335dcad3317215d9fdeb8bac5bacf9" "0466adb5554ea3055d0353d363832446cd8be7b799c39839f387abb631ea0995" "b5803dfb0e4b6b71f309606587dd88651efe0972a5be16ece6a958b197caeed8" "e8df30cd7fb42e56a4efc585540a2e63b0c6eeb9f4dc053373e05d774332fc13" "23c806e34594a583ea5bbf5adf9a964afe4f28b4467d28777bcba0d35aa0872e" "6c531d6c3dbc344045af7829a3a20a09929e6c41d7a7278963f7d3215139f6a7" "5784d048e5a985627520beb8a101561b502a191b52fa401139f4dd20acb07607" "3d54650e34fa27561eb81fc3ceed504970cc553cfd37f46e8a80ec32254a3ec3" "a82ab9f1308b4e10684815b08c9cac6b07d5ccb12491f44a942d845b406b0296" "6c98bc9f39e8f8fd6da5b9c74a624cbb3782b4be8abae8fd84cbc43053d7c175" "028c226411a386abc7f7a0fba1a2ebfae5fe69e2a816f54898df41a6a3412bb5" "613aedadd3b9e2554f39afe760708fc3285bf594f6447822dd29f947f0775d6c" default))
 '(exwm-floating-border-color "#fcfcfc")
 '(fci-rule-color "#ede3e5")
 '(helm-completion-style 'helm)
 '(highlight-tail-colors
   ((("#eef2e1" "green" "green")
     . 0)
    (("#e9f3f6" "cyan" "blue")
     . 20)))
 '(jdee-db-active-breakpoint-face-colors (cons "#d9c2c6" "#ff9940"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#d9c2c6" "#86b300"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#d9c2c6" "#abb0b6"))
 '(objed-cursor-color "#f07171")
 '(pdf-view-midnight-colors (cons "#575f66" "#fafafa"))
 '(rustic-ansi-faces
   ["#fafafa" "#f07171" "#86b300" "#f2ae49" "#399ee6" "#a37acc" "#55b4d4" "#575f66"])
 '(vc-annotate-background "#fafafa")
 '(vc-annotate-color-map
   (list
    (cons 20 "#86b300")
    (cons 40 "#aab118")
    (cons 60 "#ceaf30")
    (cons 80 "#f2ae49")
    (cons 100 "#f4a345")
    (cons 120 "#f79841")
    (cons 140 "#fa8d3e")
    (cons 160 "#dd866d")
    (cons 180 "#c0809c")
    (cons 200 "#a37acc")
    (cons 220 "#bc77ad")
    (cons 240 "#d6738f")
    (cons 260 "#f07171")
    (cons 280 "#de8082")
    (cons 300 "#cd9093")
    (cons 320 "#bca0a4")
    (cons 340 "#ede3e5")
    (cons 360 "#ede3e5")))
 '(vc-annotate-very-old-color nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
